package com.sr.apps.freightbit.util;

public class ParameterConstants {

	public final static String USER_TYPE = "USER_TYPE";
	
	public final static String STATUS = "STATUS";

    public final static String VENDOR_TYPE = "VENDOR_TYPE";

    public final static String CUSTOMER_TYPE ="CUSTOMER_TYPE";


    public final static String CUSTOMER_SEARCH="CUSTOMER_SEARCH";

    public final static String VENDOR_CLASS = "VENDOR_CLASS";

}
