package com.sr.apps.freightbit.customer.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;
import com.sr.apps.freightbit.customer.formbean.CustomerBean;
import com.sr.apps.freightbit.util.ParameterConstants;
import com.sr.biz.freightbit.core.entity.Client;
import com.sr.biz.freightbit.core.entity.Customer;
import com.sr.biz.freightbit.core.entity.Parameters;
import com.sr.biz.freightbit.core.service.ClientService;
import com.sr.biz.freightbit.core.service.CustomerService;
import com.sr.biz.freightbit.core.service.ParameterService;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ADMIN on 5/28/2014.
 */
public class CustomerAction extends ActionSupport implements Preparable{

    private List<CustomerBean> customers = new ArrayList<CustomerBean>();
    private List<Parameters> customerTypeList = new ArrayList<Parameters>();
    private List<Parameters> customerSearchType = new ArrayList<Parameters>();
    private CustomerBean customer = new CustomerBean();
    private Integer customerIdParam;
    private String keyword; //search keyword for customer
    private String searchType; // get the search type

    private CustomerService customerService;
    private ClientService clientService;
    private ParameterService parameterService;

    @Override
    public void prepare(){

        customerTypeList = parameterService.getParameterMap(ParameterConstants.CUSTOMER_TYPE);
        customerSearchType = parameterService.getParameterMap(ParameterConstants.CUSTOMER_SEARCH);
    }

    public String customerSearch(){
        if(searchType.equals("customerName")) {
            List<Customer> customerEntityList = customerService.findCustomerByName(keyword);
            for(Customer customerElem : customerEntityList) {
                customers.add(transformToFormBean(customerElem));
            }
        }else if(searchType.equals("customerId")){

            Customer customerEntity = customerService.findCustomerById(Integer.parseInt(keyword));
            customers.add(transformToFormBean(customerEntity));

        }else if(searchType.equals("customerType")){
            List<Customer> customerEntityList = customerService.findCustomerByType(keyword);
            for(Customer customerElem : customerEntityList){
                customers.add(transformToFormBean(customerElem));
            }

        }else if(searchType.equals("email")){
            Customer customerEntityList = customerService.findCustomerByEmail(keyword);
            customers.add(transformToFormBean(customerEntityList));
        }
            System.out.print(searchType);
            return SUCCESS;
    }



    public String customerDeleteExecute(){

        Customer customeEntity =  customerService.findCustomerById(customerIdParam);
        customerService.deleteCustomer(customeEntity);
        return SUCCESS;
    }
    public String customerEdit(){

        Customer customerEntity = customerService.findCustomerById(customerIdParam);
        customer = transformToFormBean(customerEntity);
        return SUCCESS;
    }

    public String customerEditExecute(){
        validateOnSubmit(customer);
        if(hasActionErrors())
            return INPUT;
        customerService.updateCustomer(transformToEntityBean(customer));
        return SUCCESS;

    }
    public String customerAdd(){
        return SUCCESS;
    }

    public String customerAddExecute() throws Exception {
        validateOnSubmit(customer);
        if (hasFieldErrors())
            return INPUT;
        customerService.addCustomer(transformToEntityBean(customer));
        return SUCCESS;
    }


    public String customerList(){
        List<Customer> customerEntityList = customerService.findAllCustomer(getClientId());
        for(Customer customerElem : customerEntityList){
            customers.add(transformToFormBean(customerElem));
        }
        return SUCCESS;
    }



    private CustomerBean transformToFormBean(Customer entity){

        CustomerBean formBean = new CustomerBean();
        formBean.setCustomerId(Long.toString(entity.getCustomerId()));
        formBean.setCustomerName(entity.getCustomerName());
        formBean.setPhone(entity.getPhone());
        formBean.setEmail(entity.getEmail());
        formBean.setWebsite(entity.getWebsite());
        formBean.setFax(entity.getFax());
        formBean.setMobile(entity.getMobile());
        formBean.setCustomerType(entity.getCustomerType());

        return formBean;
    }

    private Customer transformToEntityBean(CustomerBean formBean) {
        Customer entity = new Customer();
        Client client = clientService.findClientById(getClientId().toString());
        entity.setClient(client);
               if (formBean.getCustomerId()!=null)
                     entity.setCustomerId(new Integer(formBean.getCustomerId()));

            entity.setCustomerName(formBean.getCustomerName());
            entity.setCustomerType(formBean.getCustomerType());
            entity.setWebsite(formBean.getWebsite());
            entity.setPhone(formBean.getPhone());
            entity.setMobile(formBean.getMobile());
            entity.setFax(formBean.getFax());
            entity.setEmail(formBean.getEmail());
            entity.setDti(formBean.getDti());
            entity.setMayorsPermit(formBean.getMayorsPermit());
            entity.setAaf(formBean.getAaf());
            entity.setSignatureCard(formBean.getSignatureCard());
            entity.setCreatedBy("ADMIN");

        return entity;
    }
    private Integer getClientId() {
        Map sessionAttributes = ActionContext.getContext().getSession();
        Integer clientId = (Integer) sessionAttributes.get("clientId");
        return clientId;
    }

    public void validateOnSubmit(CustomerBean customerBean) {
        clearErrorsAndMessages();
        if(StringUtils.isBlank(customerBean.getCustomerName())){
            addFieldError("customer.customerName", getText("err.customerName.required"));
        }
        if(StringUtils.isBlank(customerBean.getPhone())){
            addFieldError("customer.phone", getText("err.phone.required"));
        }
        if(StringUtils.isBlank(customerBean.getMobile())){
            addFieldError("customer.mobile", getText("err.mobile.required"));
        }
        if(StringUtils.isBlank(customerBean.getEmail())){
            addFieldError("customer.email", getText("err.email.required"));
        }
    }


    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<Parameters> getCustomerSearchType() {
        return customerSearchType;
    }

    public void setCustomerSearchType(List<Parameters> customerSearchType) {
        this.customerSearchType = customerSearchType;
    }

    public List<CustomerBean> getCustomers() {
        return customers;
    }

    public void setCustomers(List<CustomerBean> customers) {
        this.customers = customers;
    }

    public List<Parameters> getCustomerTypeList() {
        return customerTypeList;
    }

    public void setCustomerTypeList(List<Parameters> customerTypeList) {
        this.customerTypeList = customerTypeList;
    }

    public CustomerBean getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerBean customer) {
        this.customer = customer;
    }

    public Integer getCustomerIdParam() {
        return customerIdParam;
    }

    public void setCustomerIdParam(Integer customerIdParam) {
        this.customerIdParam = customerIdParam;
    }

    public CustomerService getCustomerService() {
        return customerService;
    }

    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public ParameterService getParameterService() {
        return parameterService;
    }

    public void setParameterService(ParameterService parameterService) {
        this.parameterService = parameterService;
    }

    public ClientService getClientService() {
        return clientService;
    }

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }
}
