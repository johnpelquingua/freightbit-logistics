    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
        <%@ taglib prefix="s" uri="/struts-tags" %>
        <%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
        <%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
        <sj:head compressed='true' jquerytheme="ui-darkness"/>
            <!-- MIDDLE -->
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <h1 class="page-header">Customer Profile</h1>

            <!-- MAIN BOX -->

            <div class="main-box">
                      <div class="panel booking panel-info">
                          <div class="booking panel-heading">
                          <img src="includes/images/search.png" class="box-icon">
                            <span class="booking panel-title">Search Customer</span>

                          </div>
                          <div class="panel-body">
                            <div class="table-responsive list-table">
                           	<div class="table-responsive list-table">
                           							<table class="table table-striped table-bordered text-center" id="customer-list">
                           							  <thead>
                           								<tr class="header_center">

                           								  <th class="tb-font-black">Customer ID</th>
                           								  <th class="tb-font-black">Customer Name</th>
                           								  <th class="tb-font-black">Customer Type</th>
                           								  <th class="tb-font-black">Email Address</th>
                                                             <th class="tb-font-black">Website</th>
                           								  <th class="tb-font-black">Contact Number</th>
                           								  <th class="tb-font-black">Action</th>
                           								</tr>
                           							  </thead>
                           							  <tbody>


                                                                     <s:iterator value="customers" var="customer">

                                                                     <tr>
                                                               <td class="tb-font-black"><s:property value="customerId" /></td>
                                                               <td class="tb-font-black"><s:property value="customerName" /></td>
                                                               <td class="tb-font-black"><s:property value="customerType" /></td>
                                                               <td class="tb-font-black"><s:property value="email" /></td>
                                                               <td class="tb-font-black"><s:property value="website" /></td>
                                                               <td class="tb-font-black"><s:property value="phone" /></td>
                                                               </tr>
                                                                      </s:iterator>

                                                        <tr>

                                                        </tr>
                           							  </tbody>
                           							</table>
                  <button type="cancel" class="btn btn-default" style="float:right;margin:6px 0px 6px 0px;" id ="groups-btn">Cancel</button>
                  <s:submit name="submit" cssStyle="float:right;margin:6px 20px 6px 0px;" cssClass="btn btn-default" value="Search" />

                              </div>
                          </div>
                      </div>
                    </div>



            <!-- END OF MAIN BOX -->

            <!-- SIDEBAR -->

            <div class="sidebar-box">
                      <div class="panel booking panel-info">
                          <div class="booking panel-heading">
                          <img src="includes/images/chat.png" class="box-icon">
                            <span class="booking panel-title">Online Chat</span>

                          </div>
                          <div class="panel-body">
                            <div class="table-responsive list-table">

                              </div>
                          </div>
                      </div>
                    </div>
                </div>

            <!-- END SIDEBAR -->

            <!-- END OF THE MIDDLE -->