<%@ taglib prefix="s" uri="/struts-tags" %>

		<!-- MIDDLE -->
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
		<h1 class="page-header">Edit Vendor</h1>
		
		<!-- EDIT HERE -->


		<div class="row" >
			<div class="col-md-12">



				<div class="panel panel-info">
					<div class="panel-heading">
						<img src="includes/images/add-user.png" class="box-icon">
						<span class="panel-title">Personal Information</span>
					</div>
					<div class="panel-body">
                        <s:form theme="bootstrap" cssClass="form-horizontal" method="post" action="editVendor">
							  <div class="form-group">

							    <label class="col-sm-2 control-label">Type:</label>

							    <div class="col-sm-10">
                                      <s:select list="vendorTypeList" name="vendor.vendorType" id="vendor.vendorType" listKey="key" listValue="value" cssClass="form-control" disabled="true" ></s:select>
							    </div>

							  </div>

							  <div class="form-group">

							    <label for="vendor.vendorName" class="col-sm-2 control-label">Company:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="vendor.vendorName" id="vendor.vendorName" cssClass="form-control" placeholder="Name"/>
                                </div>

							  </div>

							  <div class="form-group">

							    <label for="vendor.vendorCode" class="col-sm-2 control-label">Code:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="vendor.vendorCode" id="vendor.vendorCode" cssClass="form-control" placeholder="Code"/>
							    </div>

							  </div>
                        </s:form>
					</div>

				</div>
				
			</div>

			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<img src="includes/images/add-user.png" class="box-icon">
						<span class="booking panel-title">Contact Person Information</span>
					</div>
					<div class="panel-body">
                       <s:form theme="bootstrap" cssClass="form-horizontal" method="post" action="editVendor">
							  <div class="form-group">

							    <label for="contact.contactLastName" class="col-sm-2 control-label">Last Name:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="contact.contactLastName" id="contact.contactLastName" cssClass="form-control" placeholder="Last Name"/>
							    </div>

							  </div>

							  <div class="form-group">

							    <label for="contact.contactFirstName" class="col-sm-2 control-label">First Name:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="contact.contactFirstName" id="contact.contactFirstName" cssClass="form-control" placeholder="First Name"/>
							    </div>

							  </div>

							  <div class="form-group">

							    <label for="contact.contactMiddleName" class="col-sm-2 control-label">Middle Name:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="contact.contactMiddleName" id="contact.contactMiddleName" cssClass="form-control" placeholder="Middle Name"/>
							    </div>

							  </div>

							  <div class="form-group">

							    <label for="contact.contactMobile" class="col-sm-2 control-label">Mobile:</label>

							    <div class="col-sm-10">
                                    <s:textfield name="contact.contactMobile" id="contact.contactMobile" cssClass="form-control" placeholder="Mobile Number"/>
							    </div>

							  </div>

                            <div class="form-group">

                                <label for="contact.contactPhone" class="col-sm-2 control-label">Phone</label>

                                <div class="col-sm-10">
                                    <s:textfield name="contact.contactPhone" id="contact.contactPhone" cssClass="form-control" placeholder="Phone Number"/>
                                </div>

                            </div>
                        </s:form>
					</div>

				</div>
				
			</div>

			<div class="col-md-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<img src="includes/images/add-user.png" class="box-icon">
						<span class="panel-title">Other Information</span>
					</div>
					<div class="panel-body">
                        <s:form theme="bootstrap" cssClass="form-horizontal" method="post" action="editVendor">
							  <div class="form-group">

							    <label for="vendor.vendorClass" class="col-sm-2 control-label">Class:</label>

							    	<div class="col-sm-10">

                                        <s:select list="vendorClassList" name="vendor.vendorClass" id="vendor.vendorClass" listKey="key" listValue="value" cssClass="form-control" ></s:select>

							    	</div>

							  </div>

							  <div class="form-group">

							    <label for="vendor.vendorStatus" class="col-sm-2 control-label">Status:</label>

							    <div class="col-sm-10">

                                    <s:select list="statusList" name="vendor.vendorStatus" id="vendor.vendorStatus" listKey="key" listValue="value" cssClass="form-control" ></s:select>

							    </div>

							  </div>
                        </s:form>
					</div>

				</div>

			</div>

		</div>

		<div class="btn-group" style="float: right;">
			<button class="btn btn-default" type="submit">Save</button>
			<button class="btn btn-default" onclick="location.href='viewVendors'">Cancel</button>
		</div>

		<div class="btn-group" style="float: left;" id="shipping-option">
			<button class="btn btn-default" onclick="location.href='vendor-vessel.jsp'">Vessel</button>
			<button class="btn btn-default" onclick="location.href='vendor-address.jsp'">Address</button>
		</div>

		<div class="btn-group" style="float: left;" id="trucking-option">
			<button class="btn btn-default" onclick="location.href='vendorAddTrucks.jsp'">Trucks</button>
			<button class="btn btn-default" onclick="location.href='vendor-driver.jsp'">Driver</button>
			<button class="btn btn-default" onclick="location.href='vendor-address.jsp'">Address</button>
		</div>

		<div class="btn-group" style="float: left;" id="selectType">
			<button class="btn btn-default" onclick="location.href='vendor-address.jsp'">Address</button>
		</div>
							      
		<!-- SIDEBAR GOES HERE -->
		
		<!-- END OF EDIT -->
		
        </div>
		
		<!-- END OF THE MIDDLE -->

  	<script>

  		$("#shipping-option").hide();
		$("#trucking-option").show();
		$("#selectType").hide();

		function EventChanged(selectEl) {

 		 var text = selectEl.options[selectEl.selectedIndex].text;

 		if (text == "Shipping"){					
			$("#shipping-option").show();
			$("#trucking-option").hide();
			$("#selectType").hide();
		} else if (text == "Trucking") {
			$("#shipping-option").hide();
			$("#trucking-option").show();
			$("#selectType").hide();
	 	} else {
	 		$("#shipping-option").hide();
			$("#trucking-option").hide();
			$("#selectType").hide();
	 	}
	 }

	</script>
